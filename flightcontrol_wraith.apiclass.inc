<?php

/**
 * @file
 * Wraith wrapper.
 */

/**
 * Class WraithApi.
 *
 * Wrapper for wraith handling.
 */
class WraithApi implements WraithApiInterface {

  /**
   * Constructor for this class.
   */
  public function __construct() {
    $this->bin = drupal_realpath(FLIGHTCONTROL_WRAITH_BIN);
    $this->workingFolder = FALSE;
    $this->error = array();
    $this->lastOutput = '';
    $this->lastStatus = '';
    $this->lastCommand = '';

    $this->cleanupFiles = array();

//    if (!is_dir($this->home)) {
//      mkdir($this->home, 0700);
//    }
    return $this;
  }

  /**
   * Cleanup junk files.
   */
  public function __destruct() {
    foreach ($this->cleanupFiles as $file) {
      // TODO doesnt work for folders.
      if (is_file($file) || is_dir($file)) {
        if (!FLIGHTCONTROL_WRAITH_NO_CLEANUP) {
          drupal_unlink($file);
        }
      }
    }
  }

  public function setCaptureConfig(array $config) {
    $this->captureConfig = $config;
    return $this;
  }

  public function getCaptureConfig() {
    return isset($this->captureConfig) ? $this->captureConfig : FALSE;
  }

  public function captureConfigToFile() {
    // TODO: make this private ??
    if ($config = $this->getCaptureConfig()) {
      $file = $this->workingFolder . '/' . uniqid() . '.yaml';
      $this->cleanupFiles[] = $file;
      $spyc = new Spyc();
      $spyc->setting_dump_force_quotes = TRUE;
      if (file_unmanaged_save_data($spyc->dump($config), $file, FILE_EXISTS_REPLACE)) {
        return $file;
      }
      throw new Exception("Unable to dump YAML to ${file}");
    }
    throw new Exception("Configuration not yet set");
  }

  public function history() {
    return $this->exec(array(
      'history',
      $this->captureConfigToFile(),
    ));
  }

  public function latest() {
    return $this->exec(array(
      'latest',
      $this->captureConfigToFile(),
    ));
  }

  private function exec($args) {
    if (!empty(FLIGHTCONTROL_WRAITH_ARGS)) {
      $args = array_merge(preg_split('/\s+/', FLIGHTCONTROL_WRAITH_ARGS), $args);
    }

    // Check binary.
    if (!file_exists($this->bin) || !is_executable($this->bin)) {
      throw new Exception("Binary not executable");
    }

    // Check working folder.
    if (!$this->getWorkingFolder()) {
      throw new Exception("Working folder not set");
    }

    // Escape and combine.
    $params = array(
      'cd',
      $this->getWorkingFolder(),
      '&&',
      $this->bin,
    );
    foreach ($args as $arg) {
      $params[] = escapeshellarg($arg);
    }
    $command = implode(' ', $params);
    // Catch error out.
    $command .= ' 2>&1';

    // Execute and set output / status etc.
    $output = array();
    $status = 1;
    $this->lastCommand = $command;
    exec($command, $output, $status);
    $this->lastOutput = implode(PHP_EOL, $output);
    $this->lastStatus = $status;

    if ($status !== 0) {
      $this->error[] = t('Unable to execute command: !c', array('!c' => $this->lastCommand));
      $this->error[] = t('Exit status: @s', array('@s' => $this->lastStatus));
      $this->error = array_merge($this->error, array($this->lastOutput));
    }

    return $this;
  }

  /**
   * Set working folder.
   *
   * @param string $path
   *   Path to work in.
   *
   * @return bool
   *   TRUE: ok. FALSE: not possible.
   */
  public function setWorkingFolder($path) {
    if ((is_dir($path) && is_writable($path)) || (mkdir($path, 0750, TRUE) && $this->cleanupFiles[] = $path)) {
      $this->workingFolder = $path;
      return $this;
    }
    else {
      throw new Exception("Unable to set working folder ${path}");
    }
  }

  /**
   * Get working folder.
   *
   * @return string|bool
   *   Path, or FALSE on failure.
   */
  public function getWorkingFolder() {
    return $this->workingFolder;
  }

}

/**
 * Interface WraithApiInterface.
 */
interface WraithApiInterface {

  public function history();

  public function latest();

  /**
   * @param array $config
   * @return WraithApiInterface
   */
  public function setCaptureConfig(array $config);

  /**
   * @return array|bool
   */
  public function getCaptureConfig();

  /**
   * @return WraithApiInterface
   */
  public function getWorkingFolder();

  /**
   * @param $path
   * @return WraithApiInterface
   */
  public function setWorkingFolder($path);

}
